import React, { Component } from 'react';
import Header from './header';
import Carousel from './carousel';
import Product from './product';
import List from './list';
import Footer from './footer';


class Page extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Carousel/>
                <Product/>
                <List/>
                <Footer/>
            </div>
        );
    }
}

export default Page;