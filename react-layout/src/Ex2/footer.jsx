import React, { Component } from 'react';
import footer from "../Ex2/footer.css";

class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <div className="text-white text-center py-3">
                Copyright by Mr.Nguyen
                </div>
            </div>
        );
    }
}

export default Footer;