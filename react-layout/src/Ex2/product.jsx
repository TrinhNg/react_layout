import React, { Component } from 'react';
import product from '../Ex2/product.css';

class Product extends Component {
    render() {
        return (
            <div className="pro_content">
                <div className="content_left">
                    <h2>What We Do</h2>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente fugit doloribus iusto quos magni cumque ea. Id voluptas totam odit in reiciendis placeat, illo quaerat officiis necessitatibus accusamus corporis optio.</p>
                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sapiente fugit doloribus iusto quos magni cumque ea. Id voluptas totam odit in reiciendis placeat, illo quaerat officiis necessitatibus accusamus corporis optio.</p>
                </div>
                <div className="content_right">
                    <h2>Contact US</h2>
                    <p>CyberSoft</p>
                    <p>Su Van Hanh, quận 10, Tp.HCM</p>
                    <p>website: cybersoft.edu.vn</p>
                </div>
            </div>
        );
    }
}

export default Product;