import React, { Component } from 'react';
import smartphone from '../Ex1/smartphone.css';
import blackberry from "../assets/img/sp_blackberry.png";
import iphoneX from "../assets/img/sp_iphoneX.png";
import note7 from "../assets/img/sp_note7.png";
import vivo850 from "../assets/img/sp_vivo850.png";

class Smartphone extends Component {
    render() {
        return (
            <div className="smartphone">
                <div>
                    <div className="smart_top pt-3">BEST SMARTPHONE</div>
                    <div className="smart_botton pt-2 pb-5">
                        <div className="card" style={{width: '19rem'}}>
                            <img src={iphoneX} />
                            <div className="card-body">
                                <h5 className="card-title">Iphone X</h5>
                                <p className="card-text">iPhone X features a new all-screen design. Face ID, which makes your face your password</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={note7} />
                            <div className="card-body">
                                <h5 className="card-title">Galaxy Note 7</h5>
                                <p className="card-text">The Galaxy Note7 comes with a perfectly symmetrical design for good reason</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={vivo850} />
                            <div className="card-body">
                                <h5 className="card-title">Vivo</h5>
                                <p className="card-text">A young global smartphone brand focusing on introducing perfect sound quality</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={blackberry} />
                            <div className="card-body">
                                <h5 className="card-title">Black Berry</h5>
                                <p className="card-text">BlackBerry is a line of smartphones, tablets, and services originally designed</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Smartphone;