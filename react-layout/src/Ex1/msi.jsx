import React, { Component } from 'react';
import Header from './header';
import Carousel from './carousel';
import Smartphone from './smartphone';
import Laptop from './laptop';
import Footer from './footer';

class Msi extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Carousel/>
                <Smartphone/>
                <Laptop/>
                <Footer/>
            </div>
        );
    }
}

export default Msi;