import React, { Component } from 'react';
import laptop from "../Ex1/laptop.css";
import hp from "../assets/img/lt_hp.png";
import lenovo from "../assets/img/lt_lenovo.png";
import macbook from "../assets/img/lt_macbook.png";
import rog from "../assets/img/lt_rog.png";

class Laptop extends Component {
    render() {
        return (
            <div> 
                <div className="laptop">
                    <div className="lap_top pt-5 text-dark">BEST LAPTOP</div>
                    <div className="smart_botton pt-2 pb-5">
                        <div className="card" style={{width: '19rem'}}>
                            <img src={macbook} />
                            <div className="card-body">
                                <h5 className="card-title">MACBOOK</h5>
                                <p className="card-text">The MacBook is a brand of notebook computers manufactured by Apple Inc</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={rog} />
                            <div className="card-body">
                                <h5 className="card-title">ASUS ROG</h5>
                                <p className="card-text">the Asus ROG Strix Flare is the latest addition to Asus' lineup of ROG branded devices</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={hp} />
                            <div className="card-body">
                                <h5 className="card-title">HP OMEN</h5>
                                <p className="card-text">A young global smartphone brand focusing on introducing perfect sound quality</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                        <div className="card" style={{width: '19rem'}}>
                            <img src={lenovo} />
                            <div className="card-body">
                                <h5 className="card-title">LENOVO THINKPAD</h5>
                                <p className="card-text">The ThinkPad X1 Carbon is a high-end notebook computer released by Lenovo in 2012</p>
                            </div>
                            <div className="card-body bottom">
                                <a href="#" className="card-link bg-primary text-white">Detail</a>
                                <a href="#" className="card-link bg-danger text-white">Cart</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Laptop;