import React, { Component } from 'react';
import header from "../Ex1/header.css"

class Header extends Component {
    render() {
        return (
            <div className="bg-dark d-flex">
                <div className="left text-white">CYPBER SOFT</div>
                <div className="right">
                     <nav className="navbar navbar-expand-lg navbar-light ">
                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav">
                            <a className="nav-link active" href="#">Home <span className="sr-only">(current)</span></a>
                            <a className="nav-link" href="#">News</a>
                            <a className="nav-link" href="#">Products</a>
                            <a className="nav-link" href="#">Forum</a>
                          
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        );
    }
}

export default Header;