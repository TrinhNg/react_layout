import React, { Component } from 'react';
import footer from "../Ex1/footer.css";
import promo1 from "../assets/img/promotion_1.png";
import promo2 from "../assets/img/promotion_2.png";
import promo3 from "../assets/img/promotion_3.jpg";

class Footer extends Component {
    render() {
        return (
            <div className="promo">
                <div className="pro pb-5" >
                    <div className="pro_top pt-4 pb-2">PROMOTION</div>
                    <div className="pro_bottom py-4">
                        <span>
                            <img src={promo1} alt="" />
                        </span>
                        <span>
                            <img src={promo2} alt="" />
                        </span>
                        <span>
                            <img src={promo3} alt="" />
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;