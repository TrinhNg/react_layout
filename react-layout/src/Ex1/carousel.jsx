import React, { Component } from 'react';
import carousel from '../Ex1/carousel.css';
import Slide1 from "../assets/img/slide_1.jpg";
import Slide2 from "../assets/img/slide_2.jpg";
import Slide3 from "../assets/img/slide_3.jpg";

class Carousel extends Component {
    state={
        isSlide:Slide1,
    }
    handSlide = (slide)=>{
        return ()=>{
            this.setState({
                isSlide:slide,
            })
        };
    }
  

    render() {
        return (
            <div>
               <div id="demo" className="carousel slide" data-ride="carousel">
 
  <ul className="carousel-indicators">
    <li data-target="#demo" data-slide-to={0} className="active" />
    <li data-target="#demo" data-slide-to={1} />
    <li data-target="#demo" data-slide-to={2} />
  </ul>
 
  <div className="carousel-inner">
    <div className="carousel-item active">
      <img src={Slide1} />
    </div>
    <div className="carousel-item">
      <img src={Slide2} />
    </div>
    <div className="carousel-item">
      <img src={Slide3} />
    </div>
  </div>

  <a className="carousel-control-prev" href="#demo" data-slide="prev">
    <span className="carousel-control-prev-icon" />
  </a>
  <a className="carousel-control-next" href="#demo" data-slide="next">
    <span className="carousel-control-next-icon" />
  </a>
</div>


            </div>
        );
    }
}

export default Carousel;